import oras
import familii

def introducere_familii(nr_fam, nume_oras) :
        i = 0
        global p
        
        for i in range (nr_fam) :
                nume_temp = input ("Nume familie: ")
                nr_copii = int (input ("Numar copii: ") )
                p.append (familii.Familie (nume_temp, nr_copii, nume_oras) )

def introducere_oras(nr_orase, o) :
        i = 0
        
        for i in range (nr_orase) :
                nume_oras = input ("Nume Oras : ")
                nr_fam = int (input ("Numar Familii : ") )
                o.append (oras.Oras (nume_oras, nr_fam) )
                introducere_familii (nr_fam, nume_oras)
        return o

def alegere_oras() :
        global o, alegere, verif
        i = 0
        verif = 0
        print ("Alegeti orasul in care doriti sa cautati familia: ")
        
        for i in range ( len (o) ) :
                print (o[i].nume)
        alegere = input ("$ ")
        verificare_aleg ()
        
        if verif == 1 :
                cautare_meniu ()
        else :
                print ("Orasul nu a fost gasit. \n")
                alegere_oras ()
                                
def verificare_aleg() :
        global o, alegere, verif
        i = 0
        
        for i in range (len (o) ) :
                if o[i].nume.upper() == alegere.upper() :
                        verif = 1
                        break
def cautare_meniu() :
        global alegere
        print ("Se cauta familii in orasul ", alegere.capitalize(), ". Doriti sa cautati dupa" \
                        " Prima litera(0), Ultima litera(1), Doua litere alaturate(2)" \
                        " sau doriti sa schimbati orasul in care cautati(3) ?")
        alegere1 = int (input ("$ ") )
        
        if alegere1 == 0 :
                cautare1()
        elif alegere1 == 1 :
                cautare2()
        elif alegere1 == 2 :
                cautare3()
        elif alegere1 == 3 :
                alegere_oras()
                
def cautare1() :
        global p, alegere
        i = 0
        aux = 0
        caut = input ("*Cautare dupa prima litera* \n Introduceti litera: ")

        for i in range (len (p) ) :

                if p[i].nume[0].upper() == caut.upper() \
                   and p[i].oras.capitalize() == alegere.capitalize() :
                        aux = 1
                        print ("\nFamilia: ", p[i].nume, "Numar copii: " \
                               , p[i].nr_copii, "Din orasul ", p[i].oras, "\n")

        if aux == 0 :
                print ("Nu a fost gasit nici un rezultat\n")
        cautare_meniu()

def cautare2() :
        global p,alegere
        aux = 0
        i = 0

        caut = input ("*Cautare dupa ultima litera* \n Introduceti litera: ")
        for i in range(len(p)) :

                if p[i].nume[-1:].upper() == caut.upper() \
                   and p[i].oras.capitalize() == alegere.capitalize() :
                        aux = 1
                        print ("\nFamilia: ", p[i].nume, "Numar copii: " \
                               , p[i].nr_copii, "Din orasul ", p[i].oras, "\n")

        if aux == 0 :
                print ("Nu a fost gasit nici un rezultat\n")
        cautare_meniu()

def cautare3() :
        global p,alegere
        aux = 0
        i = 0
        j = 0
        caut = input ("*Cautare dupa un grup de doua litere* \n Introduceti literele: ")

        for i in range ( len (p) ) :

                for j in range ( len (p[i].nume)-1 ):

                        if p[i].nume[j].upper() == caut[0].upper() and \
                           p[i].nume[j+1].upper() == caut[1].upper()\
                           and p[i].oras.capitalize() == alegere.capitalize():
                                aux = 1
                                print ("\nFamilia: ", p[i].nume, "Numar copii: " \
                                       , p[i].nr_copii, "Din orasul ", p[i].oras, "\n")
                        
        if aux == 0 :
                print ("Nu a fost gasit nici un rezultat\n")
        cautare_meniu()
nr_orase = int(input("Numar orase: "))
o = []
i = 0
verif = 0
p = []
introducere_oras(nr_orase, o)
alegere_oras()


